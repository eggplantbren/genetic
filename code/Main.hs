import Genetic.RNG
import Genetic.Genome

-- Main program for "Genetic" project

-- For random numbers
import System.Random.MWC

-- Main function
main :: IO ()
main = withSystemRandom . asGenIO $ \gen -> do
    g1 <- generateGenome 50 gen
    g2 <- generateGenome 50 gen

    putStrLn $ "Parent 1: " ++ (Genetic.Genome.show g1)
    putStrLn $ "Parent 2: " ++ (Genetic.Genome.show g2)
    child <- breed g1 g2 gen
    putStrLn $ "Child:    " ++ (Genetic.Genome.show child)

    return ()


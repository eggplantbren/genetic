module Genetic.Genome where

import Genetic.RNG

import Data.List (foldl')
import System.Random.MWC (Gen)
import Control.Monad.Primitive (RealWorld)
import qualified Data.Vector.Unboxed as U       -- Unboxed vectors

-- A Genome is just a vector of Ints (really they'll just contain 0s
-- and 1s)
type Genome = U.Vector Int

-- Generate a Genome with a specified length
-- from a uniform distribution over the space of bit sequences
generateGenome :: Int -> Gen RealWorld -> IO Genome
generateGenome n gen
    | n <= 0    = undefined
    | otherwise = do
        u <- U.replicateM n (randInt 10 gen)
        --let genome = U.map (\a -> if a < 0.5 then 0 else 1) u :: Genome
        return u --genome

-- Replace 'show'
show :: Genome -> String
show genome = foldl' (++) [] chars
    where
        chars = map Prelude.show $ U.toList genome

-- Breed two genomes together with crossover
breed :: Genome -> Genome -> Gen RealWorld -> IO Genome
breed mother father gen
    | U.length mother /= U.length father = undefined
    | otherwise = do
        let n = U.length mother

        -- Start and end point of the crossover
        a <- randInt n gen
        b <- randInt n gen
        let ab = [a, b]
        let (left, right) = (minimum ab, maximum ab)

        -- Function that takes an index as input and returns an element
        -- from either the mother or the father.
        let f i = if i >= left && i < right then (mother U.! i) else (father U.! i)
        let child = U.map f $ U.fromList [0..(n-1)]
        return child


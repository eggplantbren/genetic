Genetic
=======

Nested Sampling which uses 'breeding' when possible to initialise
the new particle.

Also, parameter vectors are implemented using a gray coding.

(c) 2016 Brendon J. Brewer
LICENCE: MIT.

